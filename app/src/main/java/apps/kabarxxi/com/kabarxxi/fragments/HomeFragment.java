package apps.kabarxxi.com.kabarxxi.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import apps.kabarxxi.com.kabarxxi.R;
import apps.kabarxxi.com.kabarxxi.adapters.TabAdapter;

public class HomeFragment extends Fragment {


    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getFragmentManager());
        adapter.addFragment(new LatestNewsFragment(), "Terbaru");
        adapter.addFragment(new MainNewsFragment(), "Berita Utama");
        adapter.addFragment(new MostPopularFragment(), "Most Popular");
        adapter.addFragment(new MostCommentedFragment(), "Most Commented");
        adapter.addFragment(new NewsTipsFragment(), "Tips");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }
}
