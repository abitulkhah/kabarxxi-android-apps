package apps.kabarxxi.com.kabarxxi.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import apps.kabarxxi.com.kabarxxi.R;

public class VideoFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setTitle("VIDEO");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        final VideoView videoView;
        videoView =  (VideoView) view.findViewById(R.id.videoView);
        videoView.setVideoPath("https://www.youtube.com/watch?v=zKVmwA5ITnw");
        return view;
    }
}
