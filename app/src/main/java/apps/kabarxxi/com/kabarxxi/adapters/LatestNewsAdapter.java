package apps.kabarxxi.com.kabarxxi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import apps.kabarxxi.com.kabarxxi.R;
import apps.kabarxxi.com.kabarxxi.models.LatestNews;

public class LatestNewsAdapter extends RecyclerView.Adapter<LatestNewsAdapter.MyViewHolder>{
    private Context mContext;
    private List<LatestNews> latestNewsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsTitle, newsTime;
        public ImageView newsImage;

        public MyViewHolder(View view) {
            super(view);
            newsTitle = (TextView) view.findViewById(R.id.newsTitle);
            newsTime = (TextView) view.findViewById(R.id.newsTime);
            newsImage = (ImageView) view.findViewById(R.id.newsImage);
        }
    }


    public LatestNewsAdapter(Context mContext, List<LatestNews> latestNewsList) {
        this.mContext = mContext;
        this.latestNewsList = latestNewsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.latest_news_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        LatestNews latestNews = latestNewsList.get(position);
        holder.newsTitle.setText(latestNews.getNewsTitle());
        holder.newsTime.setText(latestNews.getNewsTime());
        holder.newsImage.setImageDrawable(latestNews.getNewsImage());
    }



    @Override
    public int getItemCount() {
        return latestNewsList.size();
    }
}
