package apps.kabarxxi.com.kabarxxi.models;

import android.graphics.drawable.Drawable;

public class MainNews {

    private String newsTitle;
    private String newsTime;
    private Drawable drawable;

    public MainNews(String newsTitle, String newsTime, Drawable drawable) {
        this.newsTitle = newsTitle;
        this.newsTime = newsTime;
        this.drawable = drawable;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
