package apps.kabarxxi.com.kabarxxi.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apps.kabarxxi.com.kabarxxi.R;
import apps.kabarxxi.com.kabarxxi.adapters.LatestNewsAdapter;
import apps.kabarxxi.com.kabarxxi.models.LatestNews;

public class LatestNewsFragment extends Fragment {

    private RecyclerView recyclerView;
    private LatestNewsAdapter latestNewsAdapter;
    private List<LatestNews> latestNewsList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_latest_news, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_latest_news);

        latestNewsList = new ArrayList<>();
        prepareLatestNews();
        latestNewsAdapter = new LatestNewsAdapter(getContext(), latestNewsList);
        latestNewsAdapter.notifyDataSetChanged();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(latestNewsAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }

    /**
     * Adding few albums for testing
     */
    private void prepareLatestNews() {

        for (int i= 0; i < 15; i++){
            LatestNews news1 = new LatestNews(i%2==0 ? "Perang - perang ala Amin Rais" : "Komparasi Galaxy M30 dengan Rival, Termasuk Redmi Note 7 Pro", "Sabtu, 2 Maret 2019", getResources().getDrawable(i%2==0 ? R.drawable.amin : R.drawable.m30));
            latestNewsList.add(news1);

        }
    }

}
